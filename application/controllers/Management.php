<?php

    defined('BASEPATH') OR exit('No direct script access allowed');
    
    class Management extends CI_Controller {
        /**
            * Management / Business Type Index Page for this controller.
        */
        public function business_type(){
            $data['record'] = $this->model_apps->view_ordering('ref_jenisusaha','JENISUSAHA_ID','DESC');
            $this->template->load('template','management/business_type/index',$data);
        }

        /**
            * Management / Business Type Create Page for this controller.
        */
        public function create_business_type(){
            if (isset($_POST['submit'])){
                $data = array('JENISUSAHA_NAME'=>$this->input->post('a'));
                $this->model_apps->insert('ref_jenisusaha',$data);
                redirect('management/business_type');
            }else{
                $this->template->load('template','management/business_type/create');
            }
        }

        /**
            * Management / Business Type Edit Page for this controller.
        */
        public function edit_business_type(){
            
        }

        /**
            * Management / Business Type Delete Page for this controller.
        */
        public function delete_business_type(){
            
        }


        
        /**
            * Management / Business Type Index Page for this controller.
        */
        public function business_group(){
            $data['record'] = $this->model_apps->view_ordering('ref_kelompokusaha','KELOMPOKUSAHA_ID','DESC');
            $this->template->load('template','management/business_group/index',$data);
        }

        /**
            * Management / Business Type Create Page for this controller.
        */
        public function create_business_group(){
            
        }

        /**
            * Management / Business Type Edit Page for this controller.
        */
        public function edit_business_group(){
            
        }

        /**
            * Management / Business Type Delete Page for this controller.
        */
        public function delete_business_group(){
            
        }



        /**
            * Management / Floor Index Page for this controller.
        */
        public function floor(){
            $data['record'] = $this->model_apps->view_ordering('ref_lantai','LANTAI_ID','DESC');
            $this->template->load('template','management/floor/index',$data);
        }

        /**
            * Management / Floor Create Page for this controller.
        */
        public function create_floor(){
            
        }

        /**
            * Management / Floor Edit Page for this controller.
        */
        public function edit_floor(){
            
        }

         /**
            * Management / Floor Delete Page for this controller.
        */
        public function delete_floor(){
            
        }



        /**
            * Management / Trash type Index Page for this controller.
        */
        public function trash_type(){
            $data['record'] = $this->model_apps->view_ordering('ref_jenissampah','JENISSAMPAH_ID','DESC');
            $this->template->load('template','management/trash_type/index',$data);
        }

        /**
            * Management / Trash type Create Page for this controller.
        */
        public function create_trash_type(){
            
        }

        /**
            * Management / Trash type Edit Page for this controller.
        */
        public function edit_trash_type(){
            
        }

        /**
            * Management / Trash type Delete Page for this controller.
        */
        public function delete_trash_type(){
            
        }



        /**
            * Management / Ownership Index Page for this controller.
        */
        public function ownership(){
            $data['record'] = $this->model_apps->view_ordering('ref_kepemilikan','KEPEMILIKAN_ID','DESC');
            $this->template->load('template','management/ownership/index',$data);
        }

        /**
             * Management / Ownership Create Page for this controller.
        */
        public function create_ownership(){
            
        }

        /**
             * Management / Ownership Edit Page for this controller.
        */
        public function edit_ownership(){
            
        }

        /**
             * Management / Ownership Delete Page for this controller.
        */
        public function delete_ownership(){
            
        }



        /**
            * Management / Store type Index Page for this controller.
        */
        public function store_type(){
            $data['record'] = $this->model_apps->view_ordering('ref_tipetoko','TIPETOKO_ID','DESC');
            $this->template->load('template','management/store_type/index',$data);
        }

        /**
             * Management / Store type Create Page for this controller.
        */
        public function create_store_type(){
            
        }

        /**
             * Management / Store type Edit Page for this controller.
        */
        public function edit_store_type(){
            
        }

        /**
             * Management / Store type Delete Page for this controller.
        */
        public function delete_store_type(){
            
        }



        /**
            * Management / HPT Index Page for this controller.
        */
        public function hpt(){
            $data['record'] = $this->model_apps->view_ordering('ref_hpt','HPT_ID','DESC');
            $this->template->load('template','management/hpt/index',$data);
        }

        /**
            * Management / HPT Create Page for this controller.
        */
        public function create_hpt(){
            
        }

        /**
            * Management / HPT Edit Page for this controller.
        */
        public function edit_hpt(){
            
        }

        /**
            * Management / HPT Delete Page for this controller.
        */
        public function delete_hpt(){
            
        }



        /**
            * Management / HER Index Page for this controller.
        */
        public function her(){
            $data['record'] = $this->model_apps->view_ordering('ref_her','HER_ID','DESC');
            $this->template->load('template','management/her/index',$data);
        }

        /**
            * Management / HER Create Page for this controller.
        */
        public function create_her(){
            
        }

        /**
            * Management / HER Edit Page for this controller.
        */
        public function edit_her(){
            
        }

        /**
            * Management / HER Delete Page for this controller.
        */
        public function delete_her(){
            
        }

        /**
            * Management / User Index Page for this controller.
        */
        public function users(){
            $data['record'] = $this->model_apps->view_ordering('ref_users','USER_NIK','DESC');
            $this->template->load('template','users/index',$data);
        }

        /**
            * Management / Users Create Page for this controller.
        */
        public function create_users(){
            
        }

        /**
            * Management / Users Edit Page for this controller.
        */
        public function edit_users(){
            
        }

        /**
            * Management / Users Delete Page for this controller.
        */
        public function delete_users(){
            
        }

    }

    