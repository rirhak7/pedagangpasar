<!--
 *  =================================================
 *  Developers by fss
 *  Author      : RIZKI RAMDANI
 *  Email       : rizkiiramdaniii@gmail.com
 *  IG          : https://www.instagram.com/rizkiiramdani/
 *  Last Update : 17 September 2019
 *  =================================================
 -->
<!DOCTYPE html>
<html>
    <head>
        <title><?php echo $title; ?></title>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
        <!-- import script css apps-->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>/asset/vendor/linearicons/style.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>/asset/signin/css/main.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>/asset/signin/css/demo.css">
        <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700" rel="stylesheet">
        <link rel="shortcut icon" href="">
    </head>

    <body>
        <div id="wrapper">
        <div class="vertical-align-wrap">
            <div class="vertical-align-middle">
                <div class="auth-box">
                    <div class="left">
                        <div class="content">
                            <div class="header">
                                <div class="logo text-center">
                                    <h5>
                                        <b>Selamat datang di Data Base Pedagang Pasar</b>
                                        <p>Dinas Perdagangan Kabupaten Bekasi</p>
                                    </h5>
                                </div>
                                <p class="lead">Silahkan masukan username dan password</p>
                            </div>

                            <form action="<?= base_url('auth/signin'); ?>" method="post">
                                <div class="form-group">
                                    <label for="username" class="control-label sr-only">Username</label>
                                    <input type="text" class="form-control" name="a" placeholder="Username" required autofocus>
                                </div>

                                <div class="form-group">
                                    <label for="password" class="control-label sr-only">Password</label>
                                    <input type="password" class="form-control" name="b" placeholder="Password" required>
                                </div>

                                <button name='submit' type="submit" class="btn btn-primary btn-lg btn-block">Sign In</button>
                            </form>
                        </div>
                    </div>
                    <div class="right">
                        <div class="overlay"></div>
                        <div class="content text">
                            <h1 class="heading">Database Pedagang Pasar Management System </h1>
                            <small>
                            Demo akses Administrator / Dinas Perdagangan : <br>
                            username = admin <br>
                            password = secret

                            <br>
                            <br>
                            Copyright &copy; <?php echo date('Y'); ?> All right reserved.</strong>
                            </small>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
    </body>
</html>
