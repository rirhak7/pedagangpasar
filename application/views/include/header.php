<header class="main-header">
    <a href="<?php echo base_url()?>dashboard/home" class="logo">
        <span class="logo-mini">PP</span>
        <span class="logo-lg"><b>Pedagang Pasar</b></span>
    </a>

    <nav class="navbar navbar-static-top" role="navigation">
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <li class="dropdown user user-menu">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <span class="hidden-xs">Hello,  !</span>
                  </a>
                  <ul class="dropdown-menu">
                    <li class="user-header">       
                        <img src="<?php echo base_url(); ?>asset/foto_user/blank.png" class="img-circle" alt="User Image">
                    </li>
                    <li class="user-footer">
                      <div class="pull-right">
                        <a href="<?php echo base_url() ?>auth/logout" class="btn btn-default btn-flat">Sign out
                        </a>
                      </div>
                    </li>
                  </ul>
                </li>
                <li>
                </li>
            </ul>
        </div>
    </nav>
</header>

