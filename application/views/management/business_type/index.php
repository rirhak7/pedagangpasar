<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Jenis Usaha
        </h1>
        <ol class="breadcrumb">
            <li><a href="">Dashboard</a></li>
            <li><a href="">Jenis Usaha</a></li>
            <li class="active">View</li>
        </ol>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <a class='pull-right btn btn-primary btn-sm' href='<?php echo base_url(); ?>management/create_business_type'>Tambahkan Data</a>
                    </div>
                    
                    <div class="box-body table-responsive">
                        <table id="example1" class="table table-bordered table-striped" width="100%">
                            <thead>
                                <tr>
                                    <th style='width:20px'>No</th>
                                    <th>Jenis Usaha ID</th>
                                    <th>Name Jenis Usaha</th>
                                    <th>Tarif Perda</th>
                                    <th>KELOMPOKUSAHA_ID [Untuk Apa]</th>
                                    <th>JENISPASAR_ID_UTKUSAHA [Untuk Apa]</th>
                                    <th style='width:70px'>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php 
                                $no = 1;
                                foreach ($record as $row){
                                echo "
                                    <tr>
                                        <td>$no</td>
                                        <td>$row[JENISUSAHA_ID]</td>
                                        <td>$row[JENISUSAHA_NAME]</td>
                                        <td>Rp.$row[TARIFPERDA_JENISUSAHA_RUPIAH_HARI_M2]</td>
                                        <td>$row[KELOMPOKUSAHA_ID]</td>
                                        <td>$row[JENISPASAR_ID_UTKUSAHA]</td>
                                        <td><center>
                                            <a class='btn btn-success btn-xs' title='Edit Data' href='".base_url()."management/business_type/edit_business_type/$row[JENISUSAHA_ID]'><span class='glyphicon glyphicon-edit'></span></a>
                                            <a class='btn btn-danger btn-xs' title='Delete Data' href='".base_url()."management/business_type/delete_business_type/$row[JENISUSAHA_ID]' onclick=\"return confirm('Apa anda yakin untuk hapus Data ini?')\"><span class='glyphicon glyphicon-remove'></span></a>
                                        </center></td>
                                    </tr>";
                                $no++;
                                }
                            ?>
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </section>
</div>