<div class="content-wrapper">
    <section class="content-header">
        <h1>Tambah Jenis Usaha</h1>
        <ol class="breadcrumb">
            <li><a href="">Dashboard</a></li>
            <li><a href="">Tambah Jenis Usaha</a></li>
            <li class="active">View</li>
        </ol>
    </section>

    <section class="content">
        <div class="row">
            <?php 
                echo"
                <div class='col-xs-12'>
                    <div class='box'>                        
                        <div class='box-body table-responsive'>";
                            $attributes = array('class'=>'form-horizontal','role'=>'form');
                            echo form_open_multipart('management/create_business_type',$attributes); 
                        echo "<div class='col-md-12'>
                            <table class='table table-condensed table-bordered'>
                                <tbody>
                                    <input type='hidden' name='id' value=''>
                                    <tr>
                                        <th width='120px' scope='row'>Nama Jenis Usaha</th>    
                                        <td><input type='text' class='form-control' name='a' required></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class='box-footer'>    
                        <button type='submit' name='submit' class='btn btn-info'>Tambahkan</button>
                        <a href='business_type'><button type='button' class='btn btn-default pull-right'>Cancel</button></a>           
                    </div>
                </div>";
            ?>
        </div>
    </section>
</div>